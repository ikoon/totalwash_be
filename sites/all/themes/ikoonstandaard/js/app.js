(function ($) {
    Drupal.behaviors.behaviorName = {
        attach: function (context, settings) {
            var $win = $(window);

            $('#block-views-tips-block .field-type-image').on('click', function(){
                $('#block-webform-client-block-19').toggleClass('slided-open');
            });

            // Slide down
          /*
           // $('<a href="#" class="slidedown"></a>').appendTo('.slider-container');
            $('.slidedown').on('click', function(ev){
                ev.preventDefault();

                var mainContainer = $('.main-container');

                $('html,body').animate({scrollTop: mainContainer.offset().top },'slow');
            });
*/
            // Page header
            $(window).scroll(function() {

                /*var mainTop = $('.main-container').offset().top; //get the offset top of the element
                var mainOffsetTop = mainTop - $(window).scrollTop(); //position of the ele w.r.t window

                if(mainOffsetTop <= -10){
                    $('.not-logged-in #navbar').addClass('collapsedtotop');
                    $('.not-logged-in #navbar').css('top', '0');
                }

                else if(mainOffsetTop > 110){
                    $('.not-logged-in #navbar').removeClass('collapsedtotop');
                    $('.not-logged-in #navbar').css('top', 'auto');
                }*/
            });

            // Paralax
            $('#block-views-ctc-basis-pagina-block').each(function(){
                var scroll_speed = 4;
                var $this = $(this);
                $(window).scroll(function() {
                    var bgScroll = -(($win.scrollTop() - $this.offset().top)/ scroll_speed);
                    var bgPosition = 'center '+ bgScroll + 'px';
                    $this.css({ backgroundPosition: bgPosition });
                });
            });
            $('#block-views-ctc-taxonomy-term-block').each(function(){
                var scroll_speed = 4;
                var $this = $(this);
                $(window).scroll(function() {
                    var bgScroll = -(($win.scrollTop() - $this.offset().top)/ scroll_speed);
                    var bgPosition = 'center '+ bgScroll + 'px';
                    $this.css({ backgroundPosition: bgPosition });
                });
            });


            // Navigation scroll
            var didScroll;
            var lastScrollTop = 0;
            var delta = 5;
            var navbarHeight = $('#navbar').outerHeight();

            $(window).scroll(function(event){
                didScroll = true;
            });

            setInterval(function() {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 0); // Normaal interval van 250!!

            function hasScrolled() {

                // Slide up navigation
                if ($('.main-container').offset().top > 0 && $(window).width() > 767 ){


                    var st = $(this).scrollTop();

                    // Make sure they scroll more than delta
                    if (Math.abs(lastScrollTop - st) <= delta)
                        return;

                    // If they scrolled down and are past the navbar, add class .nav-up.
                    // This is necessary so you never see what is "behind" the navbar.
                    if (st > lastScrollTop && st > navbarHeight) {
                        // Scroll Down
                        $('#navbar').removeClass('nav-down').addClass('nav-up');
                    } else {
                        // Scroll Up
                        if (st + $(window).height() < $(document).height()) {
                            $('#navbar').removeClass('nav-up').addClass('nav-down');
                        }
                    }

                    lastScrollTop = st;
                }

                // Slide in content

                var blocks = [
                    '#block-views-onze-troeven-block',
                    '.page-node-10 #block-system-main > div > .row:last-of-type',
                    '.page-node-11 #block-system-main > div > .row:last-of-type',
                    '#block-views-pagina-body-onder-block',
                    '.page-node-3 #block-system-main > div > .row:last-of-type',
                    '#block-views-flashbericht-block',
                    '#block-views-tips-block',
                    '#block-views-shop-info-block',
                    '#block-views-contact-info-block',
                    '#block-views-contact-info-block-1'
                ];

                $.each(blocks, function(i, el){
                    if($(el).length) {
                        fadein($(el));
                    }
                });

            }

        }
    };

    function fadein(element){
        var y = $(this).scrollTop();
        var x = element.position();

        if (y > (x.top - 110)) { // -20 so things don't overlap
            element.addClass("in");
        }
        else {
            element.removeClass("in");
        }
    }

}(jQuery));
